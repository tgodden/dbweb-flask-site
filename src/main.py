from flask import Flask, render_template, request, jsonify, Response
from markupsafe import Markup
from flask_restful import Resource, Api
import json
import requests

"""
COMMUNICATION
"""
api_address = "http://127.0.0.1:8080"

# Get the list of all shows by contacting the API Server
def get_all_shows():
    # Make and log the URL
    url = api_address + "/shows"
    print("LOG: Contacting server at " + url)
    # Create a request
    shows = requests.get(url)
    print("LOG: Server returned " + str(shows.status_code))
    # Check request result
    if shows.status_code > 400:
        raise(Exception("Error getting data: error " + str(shows.status_code)))
    else:
        return shows.json()


"""
HTML
"""
# Show class
class Show():
    # Convert a dict of a show to a Show class
    def __init__(self, show_dict):
        self.title = show_dict["title"]
        self.number_of_tickets = show_dict["number_of_tickets"]
        self.id = show_dict["id"]

    # Generate a HTML table row for a Show
    def to_html(self):
        buy_line = '<a href="/show/' + str(self.id) + '">Buy tickets</a>' if self.number_of_tickets > 0 else 'Sold Out!'
        return "<tr>" +\
               "<td>" + self.title + "</td>" +\
               "<td>" + buy_line + "</td>" + \
               "</tr>"

# Generate HTML to display all shows in a table
def all_shows_table(shows):
    html_table = """\
<table>
 <tr>
  <th>Show</th>
  <th><Tickets Left></th>
  <th/>
 <tr>
"""
    table_end = "\n<table>"
    for show_dict in shows["shows"]:
        show = Show(show_dict)
        html_table += show.to_html()
    return Markup(html_table + table_end)

"""
PAGES
"""
# Shows page
class ShowsPageR(Resource):
    # Get the shows overview page
    def get(self):
        print(token_username)
        # See if there is a token
        token = request.cookies.get("auth_token")
        # Get the token from the database
        username = str(token_username.get(token))
        # Try getting all shows
        try:
            shows = get_all_shows()
            if token:
                # Logged in
                loggedin = "Logged in as " + username
            else:
                # Not logged in
                loggedin = "Not logged in"
            # Render the template "templates/allshows.html" with {{content}} being replaced by the table
            return Response(response=render_template("allshows.html", content=all_shows_table(shows), loggedin=loggedin))
        except Exception as e:
            # Render the template "templates/allshows.html" with {{content}} being replaced by an error message.
            # This should ideally use a different template, like an error page
            return Response(response=render_template("allshows.html", content="Error contacting database"))


import random
# Create a token_username database
token_username = dict()
# Create a username_password database
username_password = {"admin": "admin"}


# DO THIS REMOTE OBVIOUSLY
# Log a user in to the server
def register_to_server_mock(username, password):
    if username_password.get(username) == password:
        rand = random.randint(0, 2**16 - 1)
        token_username[str(rand)] = username
        return (True, json.dumps(rand).encode('utf-8'))
    else:
        return (False, -1)


# Log in page
class LoginPageR(Resource):
    # Simply shows log in form
    def get(self):
        return Response(response=render_template("login.html"))

# Log In Attempt Page
class LoginAttemptPageR(Resource):
    # Should fail, only post allowed
    def get(self):
        return Response(response=render_template("loginattempt.html", title="404: Invalid Method", contents="404: Invalid method"))

    # Returns whether or not the login was a success
    def post(self):
        data = request.form
        try:
            # Get data from form
            username = data['username']
            password = data['password']
        except KeyError:
            # Data not found in form -> 400
            response = Response(response=render_template("loginattempt.html", title="400: Bad Request", contents="400: Bad Request"))
        # Try to login at server
        (success, token) = register_to_server_mock(username, password)
        if success:
            # Succesful login -> welcome and set cookie
            response = Response(response=render_template("loginattempt.html", title="Login Succesful", contents="Welcome " + username))
            response.set_cookie("auth_token", str(token))
        else:
            # Unsuccesful log in -> tell user
            response = Response(response=render_template("loginattempt.html", title="Failed to log in", contents="Failed to log in"))
        return response


"""
ROUTING
"""
# Create our app
app = Flask(__name__)
# Create an API for our app
api = Api(app)

# Add the resources to the API and specify the path
api.add_resource(ShowsPageR, "/")
api.add_resource(LoginPageR, "/login")
api.add_resource(LoginAttemptPageR, "/login_attempt")

# Run the app
if __name__ == "__main__":
    app.run(host='localhost', port=8081)

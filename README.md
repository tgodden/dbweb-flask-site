# Example Website
Note: *This code demonstrates how to use Flask for the purpose of creating a simple site using the API written previously.
For the project, you should write your own code and use what you learn here!*

Previously, we have used Flask to design a web API which listens to GET and POST HTTP requests.
Here, we will use that API to create a site to display our shows.
We will also discuss how to add cookies to your site for the purpose of authentication.

Since this site uses the API, make sure you run the API program before running the site.


## Calling the API
In order to call the API, we use the `request` library as described in the "Testing" part of the API example.
Refer to that explanation for more information.

## Flask for web pages
Similarly to our API, we will use the `flask-restful` library to route our web page.
The difference this time, is the fact that we need to return a web page instead of just raw data.
Let's take a look at the code for returning a login page, which can be accessed at `/login`:
```python
# Log in page
class LoginPageR(Resource):
    # Simply shows log in form
    def get(self):
        return Response(response=render_template("login.html"))
...
api.add_resource(LoginPageR, "/login")
```
The `Response` class creates a HTTP response to send back to our web browser.

### HTML Templates
As seen in the previous code example, our `Response` uses the value retrieved from `render_template`.
In the `src/` directory, you can find the code and a subdirectory called `templates/`.
This directory contains templates for the HTML pages we want to display.
However, because we want to generate our page at runtime, we add variables in the HTML code.

Let's take a look at the HTML code for the root (`/`) page at `src/templates/allshows.html`:
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">
    <meta charset="UTF-8">
    <title>{{title}}</title>
</head>
<body>
{{loggedin}}
{{content}}
</body>
</html>
```
The text between `{{` and `}}` are our variables.
In the HTML code, you can see that we assign values to these variables, which are then displayed on the site.

However, we can't put HTML code as a string in our variables. In order to do this, we need to create a `Markup` object containing
the HTML string, as shown in the `all_shows_table` function.
This function takes a JSON Object containing the list of all shows which we got from the API, and translates it into
a nice HTML table:
```python
# Generate HTML to display all shows in a table
def all_shows_table(shows):
    html_table = """\
<table>
 <tr>
  <th>Show</th>
  <th><Tickets Left></th>
  <th/>
 <tr>
"""
    table_end = "\n<table>"
    for show_dict in shows["shows"]:
        show = Show(show_dict)
        html_table += show.to_html()
    return Markup(html_table + table_end)
```

### Authorisation with cookies
In order to allow users to log in, we need to register them to the server.
Because we have not implemented this in the API, we created a mock function called `register_to_server_mock`, which
will try to log in a user with a username and password.
If the username and password are in the 'database' (simply represented like a dict), we will create a random token
and return it to the site.

This token identifies the user's session. We will store it in the user's cookies to remember it.
We do this by rendering the web page template (`render_template`), creating a response with it (`make_response`) and
then adding a cookie to the response (`response.set_cookie`).

But how do we actually make this work? Well, we create an HTML form in `src/templates/login.html`,
which redirects to `/login_attempt` with a POST request, and which contains the username and password.

If you want to see what these cookies look like, you can use a browser add-on like Cookie-Editor for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/cookie-editor/) or [Chrome](https://chromewebstore.google.com/detail/cookie-editor).
